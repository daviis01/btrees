# File bttestNoDelete.py  -- Modified bttest5d.py
# 11/18/13

from btree import BTree
from person import Person
import sys

def main():
    
    # run #2
    bt = BTree(1)
    bt.insert(15)
    bt.insert(30)
    bt.insert(40)
    print( bt )

    print( ' run #3 ---------------------------------------' )
    bt = BTree(1)
    bt.insert(50)
    bt.insert(27)
    bt.insert(35)
    bt.insert(98)
    bt.insert(201)
    bt.insert(201)
    bt.insert(73)
    bt.insert(29)
    bt.insert(150)
    bt.insert(15)
    bt.insert(64)
    bt.insert(83)
    bt.insert(90)
    bt.insert(87)
    bt.insert(253)
    bt.insert(84)
    print( bt )
    bt.inorderOn(sys.stdout)
    
    print( ' run #4 ---------------------------------------' )
    bt = BTree(2)
    bt.insert(20)
    bt.insert(40)
    bt.insert(10)
    bt.insert(30)
    bt.insert(15)
    bt.insert(35)
    bt.insert(7)
    bt.insert(26)
    bt.insert(18)
    bt.insert(18)
    bt.insert(22)
    bt.insert(5)
    bt.insert(42)
    bt.insert(13)
    bt.insert(46)
    bt.insert(27)
    bt.insert(8)
    bt.insert(32)
    bt.insert(38)
    bt.insert(24)
    bt.insert(45)
    bt.insert(25)
    print( bt )
    bt.levelByLevel(sys.stdout)

    print( ' run #6 -- no run #5! ---------------------------------------' )
    bt = BTree(2)
    bt.insert(20)
    bt.insert(40)
    bt.insert(10)
    bt.insert(30)
    bt.insert(15)
    bt.insert(35)
    bt.insert(7)
    bt.insert(26)
    bt.insert(18)
    bt.insert(18)
    bt.insert(22)
    bt.insert(5)
    bt.insert(42)
    bt.insert(13)
    bt.insert(46)
    bt.insert(27)
    bt.insert(8)
    bt.insert(32)
    bt.insert(38)
    bt.insert(24)
    bt.insert(45)
    bt.insert(25)
    bt.insert(2)
    print( bt )

    print( '# run #7 ---------------------------------------' )
    bt = BTree(1)
    bt.insert(27)
    bt.insert(50)
    bt.insert(35)
    bt.insert(150)
    bt.insert(98)
    bt.insert(73)
    bt.insert(201)
    print( bt )

    print( '# run #8 ---------------------------------------' )
    bt = BTree(1)
    bt.insert(27)
    bt.insert(150)
    bt.insert(201)
    print( bt )


    

    print( '# run #11  ---------------------------------------' )
    bt = BTree(2)
    fred = Person('Joe',38)
    bt.insert(fred)
    fred.id = 99
    fred = Person('Joe',48)
    bt.insert(fred)
    fred = Person('Joe',39)
    bt.insert(fred)
    bt.insert(fred)
    fred.id = 99
    fred = Person('Joe',12)
    bt.insert(fred)
    fred = Person('Joe',35)
    bt.insert(fred)
    fred = Person('Suzie',39)
    bt.update(fred)
    fred.id = 99
    fred = Person('Willy',35)
    fred = bt.retrieve(fred)
    fred.id = 99

    print( bt )
    bt.inorderOn(sys.stdout)
    print( fred )
    bt.levelByLevel(sys.stdout)
    
       
if __name__ == '__main__': main()

''' The outputof bttestNoDelete.py:
[evaluate bttestNoDelete.py]
  The degree of the BTree is 1.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: 15
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 40
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 30
                 child: 2

 run #3 ---------------------------------------
  The degree of the BTree is 1.
  The index of the root node is 15.
The contents of the node with index 1:
   Index   0  >  child: None   item: 15
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 50
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 27
                 child: 5
The contents of the node with index 4:
   Index   0  >  child: None   item: 150
                 child: None
The contents of the node with index 5:
   Index   0  >  child: None   item: 29
                 child: None
The contents of the node with index 6:
   Index   0  >  child: 2   item: 64
                 child: 8
The contents of the node with index 7:
   Index   0  >  child: 3   item: 35
                 child: 6
The contents of the node with index 8:
   Index   0  >  child: None   item: 73
                 child: None
The contents of the node with index 9:
   Index   0  >  child: None   item: 84
                 child: None
The contents of the node with index 10:
   Index   0  >  child: 9   item: 87
                 child: 12
The contents of the node with index 11:
   Index   0  >  child: None   item: 253
                 child: None
The contents of the node with index 12:
   Index   0  >  child: None   item: 90
                 child: None
The contents of the node with index 13:
   Index   0  >  child: 4   item: 201
                 child: 11
The contents of the node with index 14:
   Index   0  >  child: 10   item: 98
                 child: 13
The contents of the node with index 15:
   Index   0  >  child: 7   item: 83
                 child: 14

An inorder traversal of the BTree:
15
27
29
35
50
64
73
83
84
87
90
98
150
201
253
 run #4 ---------------------------------------
  The degree of the BTree is 2.
  The index of the root node is 9.
The contents of the node with index 1:
   Index   0  >  child: None   item: 5
   Index   1  >  child: None   item: 7
   Index   2  >  child: None   item: 8
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 22
   Index   1  >  child: None   item: 24
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 10
   Index   1  >  child: 5   item: 20
                 child: 2
The contents of the node with index 4:
   Index   0  >  child: None   item: 32
   Index   1  >  child: None   item: 35
   Index   2  >  child: None   item: 38
                 child: None
The contents of the node with index 5:
   Index   0  >  child: None   item: 13
   Index   1  >  child: None   item: 15
   Index   2  >  child: None   item: 18
                 child: None
The contents of the node with index 6:
   Index   0  >  child: None   item: 42
   Index   1  >  child: None   item: 45
   Index   2  >  child: None   item: 46
                 child: None
The contents of the node with index 7:
   Index   0  >  child: None   item: 26
   Index   1  >  child: None   item: 27
                 child: None
The contents of the node with index 8:
   Index   0  >  child: 7   item: 30
   Index   1  >  child: 4   item: 40
                 child: 6
The contents of the node with index 9:
   Index   0  >  child: 3   item: 25
                 child: 8

A level-by-level listing of the nodes: 
The contents of the node with index 9:
   Index   0  >  child: 3   item: 25
                 child: 8
The contents of the node with index 3:
   Index   0  >  child: 1   item: 10
   Index   1  >  child: 5   item: 20
                 child: 2
The contents of the node with index 8:
   Index   0  >  child: 7   item: 30
   Index   1  >  child: 4   item: 40
                 child: 6
The contents of the node with index 1:
   Index   0  >  child: None   item: 5
   Index   1  >  child: None   item: 7
   Index   2  >  child: None   item: 8
                 child: None
The contents of the node with index 5:
   Index   0  >  child: None   item: 13
   Index   1  >  child: None   item: 15
   Index   2  >  child: None   item: 18
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 22
   Index   1  >  child: None   item: 24
                 child: None
The contents of the node with index 7:
   Index   0  >  child: None   item: 26
   Index   1  >  child: None   item: 27
                 child: None
The contents of the node with index 4:
   Index   0  >  child: None   item: 32
   Index   1  >  child: None   item: 35
   Index   2  >  child: None   item: 38
                 child: None
The contents of the node with index 6:
   Index   0  >  child: None   item: 42
   Index   1  >  child: None   item: 45
   Index   2  >  child: None   item: 46
                 child: None
 run #6 -- no run #5! ---------------------------------------
  The degree of the BTree is 2.
  The index of the root node is 9.
The contents of the node with index 1:
   Index   0  >  child: None   item: 2
   Index   1  >  child: None   item: 5
   Index   2  >  child: None   item: 7
   Index   3  >  child: None   item: 8
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 22
   Index   1  >  child: None   item: 24
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 10
   Index   1  >  child: 5   item: 20
                 child: 2
The contents of the node with index 4:
   Index   0  >  child: None   item: 32
   Index   1  >  child: None   item: 35
   Index   2  >  child: None   item: 38
                 child: None
The contents of the node with index 5:
   Index   0  >  child: None   item: 13
   Index   1  >  child: None   item: 15
   Index   2  >  child: None   item: 18
                 child: None
The contents of the node with index 6:
   Index   0  >  child: None   item: 42
   Index   1  >  child: None   item: 45
   Index   2  >  child: None   item: 46
                 child: None
The contents of the node with index 7:
   Index   0  >  child: None   item: 26
   Index   1  >  child: None   item: 27
                 child: None
The contents of the node with index 8:
   Index   0  >  child: 7   item: 30
   Index   1  >  child: 4   item: 40
                 child: 6
The contents of the node with index 9:
   Index   0  >  child: 3   item: 25
                 child: 8

# run #7 ---------------------------------------
  The degree of the BTree is 1.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: 27
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 50
   Index   1  >  child: None   item: 73
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 35
   Index   1  >  child: 2   item: 98
                 child: 4
The contents of the node with index 4:
   Index   0  >  child: None   item: 150
   Index   1  >  child: None   item: 201
                 child: None

# run #8 ---------------------------------------
  The degree of the BTree is 1.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: 27
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 201
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 150
                 child: 2

# run #11  ---------------------------------------
  The degree of the BTree is 2.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: Name: Joe Id: 12 
   Index   1  >  child: None   item: Name: Joe Id: 35 
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: Name: Suzie Id: 39 
   Index   1  >  child: None   item: Name: Joe Id: 48 
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: Name: Joe Id: 38 
                 child: 2

An inorder traversal of the BTree:
Name: Joe Id: 12 
Name: Joe Id: 35 
Name: Joe Id: 38 
Name: Suzie Id: 39 
Name: Joe Id: 48 
Name: Joe Id: 99 
A level-by-level listing of the nodes: 
The contents of the node with index 3:
   Index   0  >  child: 1   item: Name: Joe Id: 38 
                 child: 2
The contents of the node with index 1:
   Index   0  >  child: None   item: Name: Joe Id: 12 
   Index   1  >  child: None   item: Name: Joe Id: 35 
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: Name: Suzie Id: 39 
   Index   1  >  child: None   item: Name: Joe Id: 48 
                 child: None
'''
