''' File testSearchTree.py '''
from btreenode import BTreeNode

from btree import BTree
from sys import *

def main():
    t = BTree(1)
    b = BTreeNode(1)
    b.index = 3
    b.insertItem(35,1,2) 
    b.insertItem(98,2,4)
    t.nodes[3] = b
    t.rootNode = b
    t.rootIndex = 3
    t.freeIndex = 5


    b = BTreeNode(1)
    b.index = 1
    b.insertItem(27,None,None) 
    b.insertItem(29,None,None)
    t.nodes[1] = b

    b = BTreeNode(1)
    b.index = 2
    b.insertItem(50,None,None) 
    b.insertItem(73,None,None)
    t.nodes[2] = b

    b = BTreeNode(1)
    b.index = 4
    b.insertItem(150,None,None) 
    b.insertItem(201,None,None)
    t.nodes[4] = b

    print( t )

    print( t.searchTree(98) ) #{'nodeIndex': 1, 'fileIndex': 3, 'found': True}
    print( t.searchTree(50) ) #{'nodeIndex': 0, 'fileIndex': 2, 'found': True}
    print( t.searchTree(150) ) #{'nodeIndex': 0, 'fileIndex': 4, 'found': True}
    print( t.searchTree(27) ) #{'nodeIndex': 0, 'fileIndex': 1, 'found': True}
    print( t.searchTree(300) ) #{'nodeIndex': 2, 'fileIndex': 4, 'found': False}
    print( t.searchTree(15) ) #{'nodeIndex': 0, 'fileIndex': 1, 'found': False}
    print( t.searchTree(28) ) #{'nodeIndex': 1, 'fileIndex': 1, 'found': False}
    print( t.searchTree(64) ) #{'nodeIndex': 1, 'fileIndex': 2, 'found': False}
    print( t.searchTree(83) ) #{'nodeIndex': 2, 'fileIndex': 2, 'found': False}
    t.levelByLevel(stdout)
    
if __name__ == '__main__': main()

''' The output:
  The degree of the BTree is 1.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: 27
   Index   1  >  child: None   item: 29
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 50
   Index   1  >  child: None   item: 73
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 35
   Index   1  >  child: 2   item: 98
                 child: 4
The contents of the node with index 4:
   Index   0  >  child: None   item: 150
   Index   1  >  child: None   item: 201
                 child: None

{'nodeIndex': 1, 'fileIndex': 3, 'found': True}
{'nodeIndex': 0, 'fileIndex': 2, 'found': True}
{'nodeIndex': 0, 'fileIndex': 4, 'found': True}
{'nodeIndex': 0, 'fileIndex': 1, 'found': True}
{'nodeIndex': 2, 'fileIndex': 4, 'found': False}
{'nodeIndex': 0, 'fileIndex': 1, 'found': False}
{'nodeIndex': 1, 'fileIndex': 1, 'found': False}
{'nodeIndex': 1, 'fileIndex': 2, 'found': False}
{'nodeIndex': 2, 'fileIndex': 2, 'found': False}
'''

