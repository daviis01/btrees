# File bttest5c.py

from person import Person
from btree import BTree
from sys import *

def main():
   # run #1
   print( 'run #1:' )
   bt = BTree(2)
   bt.insert(38)
   bt.insert(48)
   bt.insert(39)
   bt.insert(12)
   bt.insert(35)
   print( bt )
   bt.inorderOn(stdout)
   bt.levelByLevel(stdout)

   # run #2
   print( '\n\nrun#2' )

   bt = BTree(1)
   bt.insert(Person('Joe',38))
   bt.insert(Person('Suzie',48))
   bt.insert(Person("Billy",39))
   bt.insert(Person('Tomas', 12))
   bt.insert(Person('Don', 35))
   bt.update(Person('Willy', 12))
   print( bt.retrieve(Person('',48)) )
   print( bt )
   bt.levelByLevel(stdout)
   bt.inorderOn(stdout)
   bt.delete(Person('',35))
   bt.inorderOn(stdout)
   print( bt )
                    

if __name__ == '__main__': main()

'''  The results of running main: (bttest5c.py)
>>> [evaluate bttest5c.py]
run #1:
  The degree of the BTree is 2.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: 12
   Index   1  >  child: None   item: 35
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 39
   Index   1  >  child: None   item: 48
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: 38
                 child: 2

An inorder traversal of the BTree:
12
35
38
39
48
A level-by-level listing of the nodes: 
The contents of the node with index 3:
   Index   0  >  child: 1   item: 38
                 child: 2
The contents of the node with index 1:
   Index   0  >  child: None   item: 12
   Index   1  >  child: None   item: 35
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: 39
   Index   1  >  child: None   item: 48
                 child: None


run#2
Name: Suzie Id: 48 
  The degree of the BTree is 1.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: Name: Willy Id: 12 
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: Name: Suzie Id: 48 
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: Name: Don Id: 35 
   Index   1  >  child: 4   item: Name: Billy Id: 39 
                 child: 2
The contents of the node with index 4:
   Index   0  >  child: None   item: Name: Joe Id: 38 
                 child: None

A level-by-level listing of the nodes: 
The contents of the node with index 3:
   Index   0  >  child: 1   item: Name: Don Id: 35 
   Index   1  >  child: 4   item: Name: Billy Id: 39 
                 child: 2
The contents of the node with index 1:
   Index   0  >  child: None   item: Name: Willy Id: 12 
                 child: None
The contents of the node with index 4:
   Index   0  >  child: None   item: Name: Joe Id: 38 
                 child: None
The contents of the node with index 2:
   Index   0  >  child: None   item: Name: Suzie Id: 48 
                 child: None
An inorder traversal of the BTree:
Name: Willy Id: 12 
Name: Don Id: 35 
Name: Joe Id: 38 
Name: Billy Id: 39 
Name: Suzie Id: 48 
An inorder traversal of the BTree:
Name: Willy Id: 12 
Name: Joe Id: 38 
Name: Billy Id: 39 
Name: Suzie Id: 48 
  The degree of the BTree is 1.
  The index of the root node is 3.
The contents of the node with index 1:
   Index   0  >  child: None   item: Name: Willy Id: 12 
                 child: None
The contents of the node with index 3:
   Index   0  >  child: 1   item: Name: Joe Id: 38 
                 child: 4
The contents of the node with index 4:
   Index   0  >  child: None   item: Name: Billy Id: 39 
   Index   1  >  child: None   item: Name: Suzie Id: 48 
                 child: None

>>> 
'''
